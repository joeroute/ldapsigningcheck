#define SECURITY_WIN32

#include <windows.h>
#include <security.h>
#include <wincred.h>
#include <winldap.h>
#include <stdio.h>

#ifdef BOF
#include "ldap_bofdefs.h"

#define ERROR_PRINT(msg) do {\
	BeaconPrintf(CALLBACK_ERROR, "[-] %s\n", msg);\
}while(0)

#define CHECK_AND_ERROR(x, y, msg) do { \
	if (x != y) { \
		BeaconPrintf(CALLBACK_ERROR, "[-] %s\n", msg);\
		goto error; \
	} \
} while (0)

#define NULL_AND_ERROR(x, msg) do { \
	if (NULL == x) { \
		BeaconPrintf(CALLBACK_ERROR, "[-] %s\n", msg);\
		goto error; \
	} \
} while (0)
#else
#define ERROR_PRINT(msg) do {\
	printf("[-] %s\n", msg);\
}while(0)

#define CHECK_AND_ERROR(x, y, msg) do { \
	if (x != y) { \
		printf("[-] %s\n", msg);\
		goto error; \
	} \
} while (0)

#define NULL_AND_ERROR(x, msg) do { \
	if (NULL == x) { \
		printf("[-] %s\n", msg);\
		goto error; \
	} \
} while (0)
#endif

ULONG ldapSignCheck(PWCHAR IP_dc, PWCHAR spn)
{
	SECURITY_STATUS sStatus;
	ULONG retval;
	CredHandle hCredentials = { 0 };
	PLDAP pLdapConn = NULL;
	PSecBuffer ticket;
	CtxtHandle credCtx = { 0 };
	ULONG ctxAttr = 0;
	TimeStamp expiry, tsExpiry;
	CtxtHandle credCtx_2 = { 0 };

	SecBufferDesc servRetDesc = { 0 };
	SecBuffer servRetBuf = { 0 };

	SecBufferDesc secBufDesc_preauthed = { 0 };
	SecBuffer secBuf_preauthed = { 0 };

	SecBufferDesc secBufDesc_authed = { 0 };
	SecBuffer secBuff_authed = { 0 };

	secBuf_preauthed.BufferType = SECBUFFER_TOKEN;
	secBuf_preauthed.cbBuffer = 0;
	secBuf_preauthed.pvBuffer = NULL;
	secBufDesc_preauthed.cBuffers = 1;
	secBufDesc_preauthed.pBuffers = &secBuf_preauthed;
	secBufDesc_preauthed.ulVersion = SECBUFFER_VERSION;

	secBuff_authed.BufferType = SECBUFFER_TOKEN;
	secBuff_authed.cbBuffer = 0;
	secBuff_authed.pvBuffer = NULL;
	secBufDesc_authed.cBuffers = 1;
	secBufDesc_authed.pBuffers = &secBuf_preauthed;
	secBufDesc_authed.ulVersion = SECBUFFER_VERSION;
	//Get Cred Handle
	sStatus = AcquireCredentialsHandleW(
			NULL,
			L"NTLM",
			SECPKG_CRED_OUTBOUND,
			NULL,
			NULL,
			NULL,
			NULL,
			&hCredentials,
			&tsExpiry
			);
	CHECK_AND_ERROR(sStatus, SEC_E_OK, "Error getting cred handle");

	pLdapConn = ldap_initW((PWSTR)IP_dc, 389);
	NULL_AND_ERROR(pLdapConn, "Unable to initialize LDAP connection");

	//Connect to ldap server
	retval = ldap_connect(pLdapConn, NULL);
	CHECK_AND_ERROR(retval, LDAP_SUCCESS, "Unable to connect to ldap server");

	//Initialize Security Context
	sStatus = InitializeSecurityContextW(
			&hCredentials,
			NULL,
			(SEC_WCHAR*)spn,
			ISC_REQ_ALLOCATE_MEMORY | ISC_REQ_MUTUAL_AUTH | ISC_REQ_DELEGATE,
			0,
			SECURITY_NATIVE_DREP,
			NULL,
			0,
			&credCtx,
			&secBufDesc_preauthed,
			&ctxAttr,
			&expiry);

	CHECK_AND_ERROR(sStatus, SEC_I_CONTINUE_NEEDED, "An error occured getting sec ctx");

	ticket = (PSecBuffer)secBufDesc_preauthed.pBuffers;

	BERVAL initialCreds = { 0 };
	initialCreds.bv_len = ticket->cbBuffer;
	initialCreds.bv_val = (PCHAR)ticket->pvBuffer;
	PBERVAL servResp = NULL;
	ldap_sasl_bind_sW(
			pLdapConn, // Session Handle
			L"",    // Domain DN
			L"GSSAPI", //auth type
			&initialCreds, //auth
			NULL, //ctrl
			NULL,  //ctrl
			&servResp); // response

	servRetBuf.BufferType = SECBUFFER_TOKEN;
	servRetBuf.cbBuffer = servResp->bv_len;
	servRetBuf.pvBuffer = servResp->bv_val;

	servRetDesc.ulVersion = SECBUFFER_VERSION;
	servRetDesc.cBuffers = 1;
	servRetDesc.pBuffers = &servRetBuf;

	sStatus = InitializeSecurityContextW(
			&hCredentials,
			&credCtx,
			(SEC_WCHAR*)spn,
			ISC_REQ_ALLOCATE_MEMORY | ISC_REQ_MUTUAL_AUTH | ISC_REQ_DELEGATE,
			0,
			SECURITY_NATIVE_DREP,
			&servRetDesc,
			0,
			&credCtx_2,
			&secBufDesc_authed,
			&ctxAttr,
			&expiry);
	if (sStatus != SEC_E_OK)
	{
		ERROR_PRINT("[-] Issue authing");
		goto error;
	}

	BERVAL auth_creds = { 0 };
	ticket = secBufDesc_authed.pBuffers;
	auth_creds.bv_len = ticket->cbBuffer;
	auth_creds.bv_val = (PCHAR)ticket->pvBuffer;

	ldap_sasl_bind_sW(
			pLdapConn, // Session Handle
			L"",    // Domain DN
			L"GSSAPI", //auth type
			&auth_creds, //auth
			NULL, //ctrl
			NULL,  //ctrl
			&servResp); // response
	ldap_get_optionW(pLdapConn, LDAP_OPT_ERROR_NUMBER, &retval);

error:
	FreeCredentialsHandle(&hCredentials);
	DeleteSecurityContext(&credCtx);
	DeleteSecurityContext(&credCtx_2);

	if (NULL != pLdapConn)
		ldap_unbind(pLdapConn);

	if (NULL != secBuff_authed.pvBuffer)
		FreeContextBuffer(secBuff_authed.pvBuffer);
	if (NULL != secBuf_preauthed.pvBuffer)
		FreeContextBuffer(secBuf_preauthed.pvBuffer);
	return retval;
}

#ifdef BOF
void go(char* args, int len)
{
	datap parser;
	BeaconDataParse(&parser, args, len);
	wchar_t* targetSPN = (wchar_t*)BeaconDataExtract(&parser, NULL);
	wchar_t* targetDC = (wchar_t*)BeaconDataExtract(&parser, NULL);

	if (NULL == targetSPN || NULL == targetDC)
	{
		ERROR_PRINT("Incorrect args: Expecting ZZ");
		return;
	}

	BeaconPrintf(CALLBACK_OUTPUT, "[*] Checking LDAP Signing for:\n");
	BeaconPrintf(CALLBACK_OUTPUT, "[*] DC: %S\n", targetDC);
	BeaconPrintf(CALLBACK_OUTPUT, "[*] SPN: %S\n", targetSPN);
	BeaconPrintf(CALLBACK_OUTPUT, "\nResult:\n");

	switch(ldapSignCheck(targetDC, targetSPN))
	{
		case LDAP_SUCCESS:
			BeaconPrintf(CALLBACK_OUTPUT, "[+] LDAP signing disabled.\n");
			break;

		case LDAP_INVALID_CREDENTIALS:
			ERROR_PRINT("[-] Error invalid creds. Maybe this is a local account.\n");
			break;

		case LDAP_STRONG_AUTH_REQUIRED:
			BeaconPrintf(CALLBACK_OUTPUT, "[-] LDAP signing enabled.\n");
			break;

		default:
			ERROR_PRINT("[-] Another error occured.\n");
	}
}
#endif

#ifndef BOF
int wmain(int argc, wchar_t* argv[])
{
	if (argc != 3)
	{
		printf("Usage: %s [SPN] [DC IP]\n", argv[0]);
		return -1;
	}
	wchar_t* targetSPN = argv[1];
	wchar_t* targetDC = argv[2];
	printf("[*] Checking LDAP Signing for:\n");
	printf("[*] DC: %S\n", targetDC);
	printf("[*] SPN: %S\n", targetSPN);
	printf("\nResult:\n");
	switch(ldapSignCheck(targetDC, targetSPN))
	{
		case LDAP_SUCCESS:
			printf("[+] LDAP signing disabled.\n");
			break;

		case LDAP_INVALID_CREDENTIALS:
			ERROR_PRINT("[-] Error invalid creds. Maybe this is a local account.");
			break;

		case LDAP_STRONG_AUTH_REQUIRED:
			printf("[-] LDAP signing enabled.\n");
			break;

		default:
			ERROR_PRINT("[-] Another error occured.\n");
	}
	return 0;
}
#endif

