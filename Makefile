CFLAGS := -Os -s
CXXFLAGS += -w
OUTDIR := bins
CC_x64 := x86_64-w64-mingw32-gcc
STR_x64 := x86_64-w64-mingw32-strip
CC_x86 := i686-w64-mingw32-gcc
STR_x86 := i686-w64-mingw32-strip

all:
	$(CC_x64) -DBOF -c ldapsigncheck.c $(CFLAGS) $(CXXFLAGS) -o $(OUTDIR)/ldapsigncheck.x64.o
	$(STR_x64) -N ldapsigncheck.c $(OUTDIR)/ldapsigncheck.x64.o
	$(CC_x86) -DBOF -c ldapsigncheck.c $(CFLAGS) $(CXXFLAGS) -o $(OUTDIR)/ldapsigncheck.x86.o 
	$(STR_x86) -N ldapsigncheck.c $(OUTDIR)/ldapsigncheck.x86.o
	$(CC_x64) ldapsigncheck.c -municode -lwldap32 -lsecur32 $(CFLAGS) $(CXXFLAGS) -o $(OUTDIR)/ldapsigncheck.x64.exe

bof:
	$(CC_x64) -DBOF -c ldapsigncheck.c $(CFLAGS) $(CXXFLAGS) -o $(OUTDIR)/ldapsigncheck.x64.o
	$(STR_x64) -N ldapsigncheck.c $(OUTDIR)/ldapsigncheck.x64.o
	$(CC_x86) -DBOF -c ldapsigncheck.c $(CFLAGS) $(CXXFLAGS) -o $(OUTDIR)/ldapsigncheck.x86.o 
	$(STR_x86) -N ldapsigncheck.c $(OUTDIR)/ldapsigncheck.x86.o

exe:	
	$(CC_x64) -c ldapsigncheck.c -lwldap32 -lsecur32 $(CFLAGS) $(CXXFLAGS) -o $(OUTDIR)/ldapsigncheck.x64.exe
clean:
	 rm -rf bins/
	 mkdir bins
