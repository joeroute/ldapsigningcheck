
#ifndef LDAP_BOF_H
#include <windows.h>
#include <winldap.h>
#include <security.h>
#include <wincred.h>
#include "libc.h"
#include "beacon.h"

DECLSPEC_IMPORT SECURITY_STATUS WINAPI SECUR32$AcquireCredentialsHandleW(SEC_WCHAR*, SEC_WCHAR*, ULONG, PLUID, PVOID, SEC_GET_KEY_FN, PVOID, PCredHandle, PTimeStamp);
DECLSPEC_IMPORT SECURITY_STATUS WINAPI SECUR32$InitializeSecurityContextW(PCredHandle, PCtxtHandle, SEC_WCHAR*, ULONG, ULONG, ULONG, PSecBufferDesc, ULONG, PCtxtHandle, PSecBufferDesc, PULONG, PTimeStamp);
DECLSPEC_IMPORT SECURITY_STATUS WINAPI SECUR32$FreeCredentialsHandle(PCredHandle);
DECLSPEC_IMPORT SECURITY_STATUS WINAPI SECUR32$FreeContextBuffer(PVOID);
DECLSPEC_IMPORT SECURITY_STATUS WINAPI SECUR32$DeleteSecurityContext(PCtxtHandle);

DECLSPEC_IMPORT WINBASEAPI BOOL WINAPI KERNEL32$FreeLibrary(HMODULE);
DECLSPEC_IMPORT FARPROC WINAPI KERNEL32$LoadLibraryA(LPCSTR);

DECLSPEC_IMPORT LDAP* LDAPAPI WLDAP32$ldap_initW(const PWSTR HostName, ULONG PortNumber);
DECLSPEC_IMPORT ULONG LDAPAPI WLDAP32$ldap_unbind(LDAP* ld);
DECLSPEC_IMPORT ULONG LDAPAPI WLDAP32$ldap_connect(LDAP* ld, LDAP_TIMEVAL* timeout);
DECLSPEC_IMPORT ULONG LDAPAPI WLDAP32$ldap_set_optionW(LDAP* ld, int option, const void* invalue);
DECLSPEC_IMPORT ULONG LDAPAPI WLDAP32$ldap_get_optionW(LDAP* ld, int option, void* invalue);
DECLSPEC_IMPORT ULONG LDAPAPI WLDAP32$ldap_sasl_bind_sW(LDAP* ld, const PCHAR dn, const PCHAR mechanism, const BERVAL* cred, PLDAPControlA* serverctrls, PLDAPControlA* clientctrls, PBERVAL* serverdata);


#define AcquireCredentialsHandleW SECUR32$AcquireCredentialsHandleW
#define InitializeSecurityContextW SECUR32$InitializeSecurityContextW
#define FreeCredentialsHandle SECUR32$FreeCredentialsHandle
#define FreeContextBuffer SECUR32$FreeContextBuffer
#define DeleteSecurityContext SECUR32$DeleteSecurityContext
#define FreeLibrary KERNEL32$FreeLibrary
#define LoadLibraryA KERNEL32$LoadLibraryA
#define ldap_initW WLDAP32$ldap_initW
#define ldap_unbind WLDAP32$ldap_unbind
#define ldap_connect WLDAP32$ldap_connect
#define ldap_set_optionW WLDAP32$ldap_set_optionW
#define ldap_get_optionW WLDAP32$ldap_get_optionW
#define ldap_sasl_bind_sW WLDAP32$ldap_sasl_bind_sW

#endif

