# LDAP Signing check

.o and .exe formats 

## Usage:
BOF:
```
meterpreter > execute_bof ldapsigncheck.x64.o --format-string ZZ ldap\\dc-primary.localdomain 172.16.148.145
[*] Checking LDAP Signing for:
[*] DC: 172.16.148.145
[*] SPN: ldap\dc-primary.localdomain
LDAP signing disabled.
```
EXE:
```
.\ldapsigncheck.x64.exe ldap\dc-primary.localdomain 172.16.148.145
[*] Checking LDAP Signing for:
[*] DC: 172.16.148.145
[*] SPN: ldap\dc-primary.localdomain
[+] LDAP signing disabled.
```
yw @GuhnooPlusLinux ;)

